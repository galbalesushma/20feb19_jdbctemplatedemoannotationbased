<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Page</title>
</head>
<body>

<form:form modelAttribute="Department">
<table border="1">
  <thead>
    <td>
      <th>Dept-Id</th>
      <th>Dept-Name</th>
      <th>Dept-Description</th>
       <th colspan="2" >Dept-Action</th>

    </td>
  </thead>
  <tbody>
     <c:forEach items="${DepartmentListKey}" var="temp">
       <tr><td></td>
         <td>${temp.deptId}</td>
         <td>${temp.name}</td>
         <td>${temp.description}</td>
      <td><a href="/addEmp/${temp.deptId}">Add</a>
       <a href="/viewEmp/${temp.deptId}">View</a>
       </tr>
       
       
     </c:forEach >
  </tbody>
</table>
</form:form>
</body>
</html>