<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Page</title>
</head>
<body>



	<form:form modelAttribute="Employee" method="POST" action="/save">
		<table>
			<tr>
				<td>Employee-Name:</td>
				<td><form:input path="name" /></td>
			</tr>
			<tr>
				<td>Employee-Dept-Id:</td>
			
				<td><form:input path="dId" value="${dId}" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Add" /></td>
			</tr>
		</table>

	</form:form>

</body>
</html>