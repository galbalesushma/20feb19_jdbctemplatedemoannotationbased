package com.agile.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.agile.model.Department;

@Repository
public class DepartmentDaoImpl {
	@Autowired
    JdbcTemplate jdbcTemplate;
	
	 public void insertDepartment(Department department) {
	        jdbcTemplate.update("insert into department (name,description) VALUES ( ?, ?)",
	        		 department.getName(),department.getDescription());
	        System.out.println("Department Added!!");
	    }

	public List<Department> displayDeptList()
	{
		String SQL = "select * from department";
	      List <Department> departments = jdbcTemplate.query(SQL, new DepartmentMapper());
	      return departments;
	}
}
