package com.agile.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.agile.model.Department;
import com.agile.model.Employee;

@Repository

public class EmployeeDaoImpl {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public void insertEmployee(Employee employee) {
		jdbcTemplate.update("insert into employee (name,dId) VALUES ( ?, ?)", employee.getName(), employee.getdId());
		System.out.println("Employee Added!!");
	}

	public List<Employee> displayEmpList() {
		String SQL = "select * from employee";
		List<Employee> employees = jdbcTemplate.query(SQL, new EmployeeMapper());
		return employees;
	}
	
	public List<Employee> listEmployeesByDeptId(int id) {
		String SQL = "select * from employee where dId=" + id;
		List<Employee> employees = jdbcTemplate.query(SQL, new EmployeeMapper());
		return employees;
	}
}
