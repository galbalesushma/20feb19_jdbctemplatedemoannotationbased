package com.agile.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.agile.model.Department;

public class DepartmentMapper implements RowMapper<Department>{

	@Override
	public Department mapRow(ResultSet rs, int rowNum) throws SQLException {
		Department department = new Department(rs.getInt("deptId"),rs.getString("name"),rs.getString("description"));
		
	      return department;
	
	}

}
