package com.agile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DepartmentDetailsJdbcTemplateAnnotationBasedApplication {

	public static void main(String[] args) {
		SpringApplication.run(DepartmentDetailsJdbcTemplateAnnotationBasedApplication.class, args);
	}

}
