package com.agile.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.agile.model.Department;
import com.agile.model.Employee;
import com.agile.service.DepartmentServiceImpl;
import com.agile.service.EmployeeServiceImpl;

@Controller
public class HomeController {
	@Autowired
	Department department;
	@Autowired
	Employee employee;

	@Autowired
	DepartmentServiceImpl departmentServiceImpl;
	@Autowired
	EmployeeServiceImpl employeeServiceImpl;

	@RequestMapping("/")
	public String showHome(Model model) {
		model.addAttribute("Department", department);

		return "home";
	}

	@RequestMapping(value = "/addDept", method = RequestMethod.POST)
	public String addDept(@RequestParam("name") String name, @RequestParam("description") String description,
			ModelMap model) {
		department.setName(name);
		department.setDescription(description);
		departmentServiceImpl.addDepartment(department);
		model.addAttribute("msg", "Department addedd Successfully");

		return "redirect:viewDept";
	}

	@RequestMapping(value = "/viewDept")
	public String viewDeptList(ModelMap model) {
		List<Department> list = departmentServiceImpl.displayDeptList();
		model.put("DepartmentListKey", list);
		return "deptList";
	}

	@RequestMapping(value = "/addEmp/{deptId}")
	public String show(@PathVariable("deptId") int dId, Model model) {
		model.addAttribute("Employee", employee);
		model.addAttribute("dId", dId);
		return "addEmployee";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String addEmployee(@RequestParam("name") String name, @RequestParam("dId") int dId) {
		employee.setName(name);
		employee.setdId(dId);
		employeeServiceImpl.addEmployee(employee);
		return "redirect:viewDept";
	}

	@RequestMapping(value = "/viewEmp/{deptId}")
	public String showEmployeelist(@PathVariable("deptId") int dId, ModelMap model) {

		List<Employee> list = employeeServiceImpl.getEmplyeeByDept(dId);

		model.put("EmployeeListKey", list);
		return "empList";
	}

}
