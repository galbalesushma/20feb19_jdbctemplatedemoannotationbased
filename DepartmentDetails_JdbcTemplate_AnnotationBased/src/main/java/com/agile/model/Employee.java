package com.agile.model;

import org.springframework.stereotype.Component;

@Component
public class Employee {
	private int empId;

	public Employee() {

	}

	public Employee(int empId, String name, int dId) {
		super();
		this.empId = empId;
		this.name = name;
		this.dId = dId;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", name=" + name + ", dId=" + dId + "]";
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getdId() {
		return dId;
	}

	public void setdId(int dId) {
		this.dId = dId;
	}

	private String name;
	private int dId;
}
