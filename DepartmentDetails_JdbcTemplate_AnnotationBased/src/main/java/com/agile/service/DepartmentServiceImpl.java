package com.agile.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agile.dao.DepartmentDaoImpl;
import com.agile.model.Department;

@Service
public class DepartmentServiceImpl {

	@Autowired
	DepartmentDaoImpl departmentDaoImpl;

	public void addDepartment(Department department) {
		departmentDaoImpl.insertDepartment(department);

	}

	public List<Department> displayDeptList() {
		List<Department> list =departmentDaoImpl.displayDeptList();
		return list;
	}
}
