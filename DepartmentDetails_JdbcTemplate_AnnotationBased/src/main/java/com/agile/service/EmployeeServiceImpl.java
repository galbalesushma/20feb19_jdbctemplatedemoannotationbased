package com.agile.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agile.dao.EmployeeDaoImpl;
import com.agile.dao.EmployeeMapper;
import com.agile.model.Department;
import com.agile.model.Employee;

@Service

public class EmployeeServiceImpl {
	@Autowired
	EmployeeDaoImpl employeeDaoImpl;

	public void addEmployee(Employee employee) {
		employeeDaoImpl.insertEmployee(employee);
	}
	
	public List<Employee> displayDeptList() {
		List<Employee> list =employeeDaoImpl.displayEmpList();
		return list;
	}
public List<Employee> getEmplyeeByDept(int id) {
		
		return employeeDaoImpl.listEmployeesByDeptId(id);
	}
	
}
